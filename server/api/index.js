export default function(req, res, next) {
  const data = {
    error: 'Uh oh, an unexpected error has occurred.'
  }

  res.status(500).json(data)
}
