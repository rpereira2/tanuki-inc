/**
 * Newly added requires
 */
const Register = require('prom-client').register
const Summary = require('prom-client').Summary
const ResponseTime = require('response-time')
const consola = require('consola')

let responses

/**
 * A Prometheus summary to record the HTTP method, path, response code and response time
 */
module.exports.responses = responses = new Summary({
  name: 'tanuki_responses_ms',
  help: 'Response time in millis',
  labelNames: ['method', 'path', 'status']
})

/**
 * This funtion will start the collection of metrics and should be called from within in the main js file
 */
module.exports.startCollection = () => {
  consola.info(
    `Starting the collection of metrics, the metrics are available on /metrics`
  )
  require('prom-client').collectDefaultMetrics()
}

/**
 * This function increments the counters that are executed on the response side of an invocation
 * Currently it updates the responses summary
 */
module.exports.responseCounters = ResponseTime((req, res, time) => {
  consola.info('reached responseCounters middleware', req.url)
  if (req.url !== '/metrics') {
    responses.labels(req.method, req.url, res.statusCode).observe(time)
  }
})

/**
 * In order to have Prometheus get the data from this app a specific URL is registered
 */
module.exports.injectMetricsRoute = (app) => {
  consola.info('Creating metrics handler')
  app.use('/metrics', (req, res) => {
    consola.info('/metrics called')
    res.set('Content-Type', Register.contentType)
    res.end(Register.metrics())
  })
}
